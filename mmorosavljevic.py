# Import libraries
import cv2
import numpy as np

def detect_cars(frame) {
    car_cascade_src = './resources/cars.xml'
    car_cascade = cv2.CascadeClassifier(car_cascade_src)

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (5, 5), 0)
    dilated = cv2.dilate(blur, np.ones((3, 3)))
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2))
    closing = cv2.morphologyEx(dilated, cv2.MORPH_CLOSE, kernel)

    # Detects cars of different sizes in the input image
    cars = car_cascade.detectMultiScale(closing, 1.6,1)

    return cars
}

def draw_cars(frame, cars) {
    # To draw a rectangle in each cars
    for (x, y, w, h) in cars:
    cv2.rectangle(frames, (x, y), (x + w, y + h), (0, 0, 255), 2)
}
