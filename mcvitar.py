import cv2
import numpy as np

def grey(image):
    image = np.asarray(image)
    return cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

def gauss(image):
    return cv2.GaussianBlur(image, (5, 5), 0)


def canny(image):
    edges = cv2.Canny(image, 50, 110)
    return edges

def region(image):
    height, width = image.shape
    triangle = np.array([
        [(100, height-90), (width//2-40, height//2), (width-100, height-90)]
    ])
    mask = np.zeros_like(image)
    mask = cv2.fillPoly(mask, triangle, 255)
    mask = cv2.bitwise_and(image, mask)
    mask = cv2.bitwise_and(image, mask)
    return mask

def display_lines(image, lines):
    lines_image = np.zeros_like(image)
    if lines is not None:
        for line in lines:
            try:
                x1, y1, x2, y2 = line
                cv2.line(lines_image, (int(x1), int(y1)), (int(x2), int(y2)), (255, 0, 0), 5)
            except:
                print("Greška u display_lines")
    return lines_image

def average(image, lines):
    left = []
    right = []
    try:

        if lines is not None:
            for line in lines:
                x1, y1, x2, y2 = line.reshape(4)
                parameters = np.polyfit((x1, x2), (y1, y2), 1)
                slope = parameters[0]
                y_int = parameters[1]
                if slope < 0:
                    left.append((slope, y_int))
                else:
                    right.append((slope, y_int))

        right_avg = np.average(right, axis=0)
        left_avg = np.average(left, axis=0)
        left_line = make_points(image, left_avg)
        right_line = make_points(image, right_avg)
        return np.array([left_line, right_line])
    except ValueError:
        print("Greška ValueType, Left line:", left_avg, " Right line:", right_avg)


def make_points(image, average):
    #print(average)
    try:
        slope, y_int = average
        y1 = image.shape[0]
        y2 = int(y1 * (3 / 5))
        x1 = int((y1 - y_int) // slope)
        x2 = int((y2 - y_int) // slope)
        return np.array([x1, y1, x2, y2])
    except TypeError:
        print('average')

'''##### DETECTING lane lines in image ######'''
def process_frame(frame):
    copy = np.copy(frame)
    #cv2.imshow('copy',copy)
    imagedouble = copy
    edges = canny(imagedouble)
    #cv2.imshow('Edges',edges)
    isolated = region(edges)
    cv2.imshow('Isolated', isolated)
    lines = cv2.HoughLinesP(isolated, 2, np.pi / 180, 100, np.array([]), minLineLength=40, maxLineGap=5)
    averaged_lines = average(copy, lines)
    black_lines = display_lines(copy, averaged_lines)
    lanes = cv2.addWeighted(copy, 0.8, black_lines, 1, 1)
    return lanes


def videoLanes(pathToFile):
    cap = cv2.VideoCapture(pathToFile)
    while (cap.isOpened()):
        ret, frame = cap.read()
        frame = process_frame(frame)
        cv2.imshow('Lanes Detection', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()