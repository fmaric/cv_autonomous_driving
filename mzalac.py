def mzalac_project():
    import cv2
    import pytesseract
    from pytesseract import Output

    pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'

    img = cv2.imread("znak.jpg")

    def convert_grayscale(img):
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        return img

    def blur(img, param):
        img = cv2.medianBlur(img, param)
        return img

    def thresholding(img):
        img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        return img

    h, w, c = img.shape

    boxes = pytesseract.image_to_boxes(img)

    for b in boxes.splitlines():
        b = b.split(' ')
        img = cv2.rectangle(img, (int(b[1]), h - int(b[2])), (int(b[3]), h - int(b[4])), (0, 255, 0), 2)

    d = pytesseract.image_to_data(img, output_type=Output.DICT)
    print(d.keys())

    n_boxes = len(d['text'])

    for i in range(n_boxes):
        if int(float(d['conf'][i])) > 60:
            (x, y, w, h) = (d['left'][i], d['top'][i], d['width'][i], d['height'][i])
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)

    custom_config = r'-l hrv --psm 1 --oem 1'
    #print(pytesseract.image_to_string(img, config=custom_config))
    res = pytesseract.image_to_string(img, config=custom_config)
    print(res)

    cv2.imshow('img', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return rez


