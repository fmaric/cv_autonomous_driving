import cv2
import numpy as np

font = cv2.FONT_HERSHEY_PLAIN

def analyze_traffic_light(image, fjurusic_coords):
    for coords in fjurusic_coords:
        traffic_light = image[coords[1]:coords[3], coords[0]:coords[2]]

        # Convert BGR to HSV
        hsv = cv2.cvtColor(traffic_light, cv2.COLOR_BGR2HSV)
        
        # define red color range
        light_red = np.array([155, 155, 84])
        dark_red = np.array([180, 255, 255])

        # define yellow color range
        light_yellow = np.array([10, 65, 0])
        dark_yellow = np.array([40, 255, 255])

        # define green color range
        light_green = np.array([35, 15, 50])
        dark_green = np.array([90, 255, 255])

        # Threshold the HSV image to get only red colors
        mask1 = cv2.inRange(hsv, light_red, dark_red)

        # Threshold the HSV image to get only yellow colors
        mask2 = cv2.inRange(hsv, light_yellow, dark_yellow)

        # Threshold the HSV image to get only green colors
        mask3 = cv2.inRange(hsv, light_green, dark_green)

        # Bitwise-AND mask and original image
        output1 = cv2.bitwise_and(traffic_light, traffic_light, mask=mask1)
        output2 = cv2.bitwise_and(traffic_light, traffic_light, mask=mask2)
        output3 = cv2.bitwise_and(traffic_light, traffic_light, mask=mask3)

        red_pixel = np.count_nonzero(output1)
        yellow_pixel = np.count_nonzero(output2)
        green_pixel = np.count_nonzero(output3)

        label = ""
        color = (0, 0, 0)

        if red_pixel > yellow_pixel and red_pixel > green_pixel:
            label = "Red"
            color = (0, 0, 255)
        elif yellow_pixel > red_pixel and yellow_pixel > green_pixel:
            label = "Yellow"
            color = (0, 255, 255)
        elif green_pixel > yellow_pixel and green_pixel > yellow_pixel:
            label = "Green"
            color = (0, 255, 0)

        x = round((coords[0] + coords[2])/2 - 10)
        y = round((coords[1] + coords[3])/2)

        cv2.putText(image, label, (x, y), font, 1, color, 2)