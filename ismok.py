import numpy as np
import cv2 as cv

NMS_THRESHOLD = 0.3   #Threshold for separating overlapping detection
MIN_CONFIDENCE = 0.5  #Threshold to filter out detections with low confidence by the model

#Paths for model weights and configuration files
weights_path = "./resources/ismok/yolov4-tiny.weights"
config_path = "./resources/ismok/yolov4-tiny.cfg"

#Loading the model
model = cv.dnn.readNetFromDarknet(config_path, weights_path)

#Accessing the name of the last layer of the model
layer_name = model.getLayerNames()
layer_name = [layer_name[i - 1] for i in model.getUnconnectedOutLayers()]

#Function for detecting pedestrians in an image
def detect_pedestrians(image):
	#Getting image resolution
	(H, W) = image.shape[:2]
	results = []

	#Making 4D blob from an image
	blob = cv.dnn.blobFromImage(image, 1 / 255.0, (416, 416), swapRB=True, crop=False)
	model.setInput(blob)

	#Accessing the output of wanted layer
	layerOutputs = model.forward(layer_name)

	boxes = []
	centroids = []
	confidences = []

	#Looping through detections
	for output in layerOutputs:
		for detection in output:

			#Accessing confidence score for the current detection
			scores = detection[5:]
			confidence = scores[0]

			#Saving the results of the current detection
			if confidence > MIN_CONFIDENCE:
				box = detection[0:4] * np.array([W, H, W, H])
				(centerX, centerY, width, height) = box.astype("int")

				x = int(centerX - (width / 2))
				y = int(centerY - (height / 2))

				boxes.append([x, y, int(width), int(height)])
				centroids.append((centerX, centerY))
				confidences.append(float(confidence))

	#Filtering results for the current image with non maximum supression
	filteredBoxes = cv.dnn.NMSBoxes(boxes, confidences, MIN_CONFIDENCE, NMS_THRESHOLD)

	#Storing detection results into an output variable and returning them out of the function
	if len(filteredBoxes) > 0:
		for i in filteredBoxes.flatten():
			(x, y) = (boxes[i][0], boxes[i][1])
			(w, h) = (boxes[i][2], boxes[i][3])
			result = (confidences[i], (x, y, x + w, y + h), centroids[i])
			results.append(result)

	return results


def draw_pedestrian_boxes(image, results):
    #Drawing rectangles on the image with results from the detect_pedestrians function
	for res in results:
		cv.rectangle(image, (res[1][0], res[1][1]), (res[1][2], res[1][3]), (0, 255, 0), 2)